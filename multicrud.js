
$(document).ready(function(){

  var getElementValue = function(input) {
    if ($(input).is(':checkbox')) {
      return $(input).attr('checked');
    } else {
      return $(input).val();
    }
  };
  
  var NameBuilder = function(basename) {
    var items_basename = basename + '[items]';
    var delete_name = basename + '[delete]';
    this.buildElementName = function(old_element_name, item_key) {
      if (old_element_name.substring(0, basename.length) !== basename) {
        throw "old_element_name needs to begin with basename.";
      }
      var m = old_element_name.substr(basename.length).match(/^\[[\w\d_]+\]/);
      switch (m[0]) {
        case '[items]':
          var old_subname = old_element_name.substr(items_basename.length);
          var new_subname = old_subname.replace(/^\[[\w\d_]+\]/, '['+item_key+']');
          return items_basename + new_subname;
        case '[delete]':
        default:
          return false;
      }
    };
  };
  
  var RowKeeper = function(empty_row, name_builder, tbody) {
    
    var ref_values = {};
    $('input, textarea, select', empty_row).each(function(){
      var element_name = $(this).attr('name');
      ref_values[element_name] = getElementValue(this);
    });
    var row_keeper = this;
    
    this.buildRow = function(row_key) {
      var new_row = empty_row.clone();
      var name;
      $('input, textarea, select', new_row).each(function(){
        var element_name = name_builder.buildElementName($(this).attr('name'), row_key);
        if (element_name) {
          $(this).attr('name', element_name);
        } else if ($(this).val() === 'new') {
          $(this).val(row_key);
        }
      });
      new_row
        .addClass(row_key)
        .addClass('empty')
        .removeClass('new')
      ;
      return new_row;
    };
    
    this.rowIsEmpty = function(tr) {
      var is_empty = true;
      $('input, textarea, select', tr).each(function(){
        var ref_name = name_builder.buildElementName($(this).attr('name'), 'new');
        if (ref_name) {
          var value = getElementValue(this);
          if (value !== null && value !== undefined && value !== ref_values[ref_name]) {
            is_empty = false;
          }
        }
      });
      return is_empty;
    };
    
    this.updateRowClass = function(tr) {
      if (row_keeper.rowIsEmpty(tr)) {
        $(tr).addClass('empty');
      } else {
        $(tr).removeClass('empty');
      }
    };
    
    this.updateRowClasses = function() {
      $('tr.new-item', tbody).each(function(){
        row_keeper.updateRowClass(this);
      });
    };
    
    this.pruneRows = function(rowInitCallback) {
      var n_empty = 0;
      var new_rows = $('tr.new-item', tbody);
      new_rows.each(function(){
        n_empty = row_keeper.rowIsEmpty(this) ? n_empty+1 : 0;
      });
      if (n_empty > 1) {
        new_rows.slice(1-n_empty).each(function(){
          $(this).remove();
        });
      } else if (n_empty < 1) {
        var row_key = buildRowKey(tbody);
        var new_row = row_keeper.buildRow(row_key);
        tbody.append(new_row);
        rowInitCallback(new_row);
      }
    };
  };
  
  var buildRowKey = function(tbody) {
    for (var i=1; $('tr.new'+i, tbody).size(); ++i) {}
    return 'new'+i; 
  };
  
  $('tbody.multicrud-info').each(function(){
    var tbody = $(this).next('tbody');
    var multicrud_info = eval('('+$('input.multicrud-info', this).val()+')');
    var empty_row_html = multicrud_info.empty_row_html;
    var empty_row = $(empty_row_html);
    var basename = multicrud_info.basename;
    
    var name_builder = new NameBuilder(basename);
    var row_keeper = new RowKeeper(empty_row, name_builder, tbody);
    
    var update = function(){
      row_keeper.pruneRows(rowInitCallback);
      $('a.discard-button', $('tr.new-item td.delete', tbody).slice(-1)).css('display', 'none');
      $('a.discard-button', $('tr.new-item td.delete', tbody).slice(0, -1)).css('display', null);
    };
    
    var rowInitCallback = function(tr){
      $('input, textarea, select', tr)
        .click(update)
        .change(update)
        .keyup(update)
      ;
      if ($(tr).is('.new-item')) {
        var discard_button = $('<a href="" class="discard-button">discard</a>');
        $('td.delete', tr).append(discard_button);
        discard_button.click(function(event){
          $(tr).remove();
          update();
          return false;
        });
      }
    };
    
    $('tr', tbody).each(function(){
      rowInitCallback(this);
    });
    
    update();
  });
});